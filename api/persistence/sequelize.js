const Sequelize = require('sequelize');

const UserModelDefinition = require('./models/user');
const TodoModelDefinition = require('./models/todo');

const log = require('../util/log');

const { DATABASE_URL } = process.env;
const dialectOptions = {};
const sslDialectOptions = {
    require: true,
    rejectUnauthorized: false
};
if (!DATABASE_URL.includes('localhost')) {
    dialectOptions.ssl = sslDialectOptions;
}

// TODO: Check that dates are being read by sequelize in the proper timezone
const sequelizeInstance = new Sequelize(DATABASE_URL, {
    dialect: 'postgres',
    dialectOptions,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    timezone: '+00:00',
    logging: msg => log.debug(msg)
});

const UserModel = UserModelDefinition(sequelizeInstance);
const TodoModel = TodoModelDefinition(sequelizeInstance);

// set user and car relationship
UserModel.hasMany(TodoModel, {
    as: 'todos',
    foreignKey: 'userId',
    onDelete: 'CASCADE',
    hooks: true
});
TodoModel.belongsTo(UserModel, {
    as: 'user',
    foreignKey: 'userId'
});

log.info('Waiting on database sync...');
sequelizeInstance.sync().then(() => log.info('DB Sync Complete'));

module.exports = {
    UserModel,
    TodoModel
};
