const express = require('express');
const router = express.Router();

const baseService = require('../service/baseService');

router.get('/', (req, res) => {
    baseService.getGreeting(req, res);
});

module.exports = router;
