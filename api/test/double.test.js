const { assert } = require('chai');
const double = require('../util/double');

describe('simple double test', () => {
    it('should return a doubled value for a positive integer', () => {
        const result = double(12);
        assert.strictEqual(result, 24);
    });
});
