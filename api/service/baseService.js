const getGreeting = (req, res) => {
    res.status(200).json({ greeting: 'Hello World' });
};

module.exports = {
    getGreeting
};
