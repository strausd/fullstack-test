const baseController = require('../controllers/baseController');

module.exports = app => {
    app.use('/api/v1', baseController);
};
