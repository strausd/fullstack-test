import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import HomePage from '../components/pages/HomePage';
import routes from './routes';

const AppRouter = () => {
    return (
        <Router>
            <div>
                <Route path={routes.root} exact component={HomePage} />
            </div>
        </Router>
    );
};

export default AppRouter;
