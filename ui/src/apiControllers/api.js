
class Api {
    baseUrl = '/api/v1';

    getHeaders = () => {
        return {
            'Content-Type': 'application/json'
        };
    };

    getFetchData = (url, options) => {
        // translate body object to JSON for fetch to work properly
        if (typeof options.body === 'object') {
            options.body = JSON.stringify(options.body);
        }
        return fetch(`${this.baseUrl}${url}`, {
            ...options,
            headers: this.getHeaders()
        }).then(response => {
            console.log(url, response.status);
            return response.json();
        });
    };

    getGreeting = () => {
        return this.getFetchData('/', {
            method: 'get'
        });
    };
}

export default new Api();
