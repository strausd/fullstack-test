import React from 'react';

const HomePage = () => {
    return (
        <div className="home">
            <div className="header">
                <h1>Home</h1>
            </div>
        </div>
    );
};

export default HomePage;
