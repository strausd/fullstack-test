import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import dayjs from './misc/dayjs';

import 'normalize.css';

console.log(dayjs().format('hh:mm:ssa MMM D'));

ReactDOM.render(<App />, document.getElementById('root'));
