import double from '../../src/misc/double';

describe('double tests', () => {
    test('test the double function with a positive integer', () => {
        const result = double(12);
        expect(result).toBe(24);
    });
});