const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const dotenv = require('dotenv');

const baseConfig = require('./webpack.config');

// Bring in environment variables
const env = dotenv.config().parsed;
const environmentVariables = {};
Object.keys(env).forEach(key => {
    environmentVariables[`process.env.${key}`] = JSON.stringify(env[key]);
});

const devConfig = {
    mode: 'development',
    devtool: 'eval-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true,
        port: 8000,
        host: '127.0.0.1',
        publicPath: '/dist/',
        hot: true,
        lazy: false,
        disableHostCheck: true,
        proxy: {
            '/api': {
                target: 'http://localhost:8001'
            }
        },
        watchOptions: {
            aggregateTimeout: 300,
            poll: true
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin(environmentVariables)
    ]
};

module.exports = merge(baseConfig, devConfig);
