module.exports = {
    roots: ['tests'],
    setupFilesAfterEnv: ['<rootDir>/tests/setup/test-setup.js'],
    verbose: true,
    moduleDirectories: [
        'node_modules',
        'tests/setup'
    ]
};
