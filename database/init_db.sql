SET TIME ZONE 'UTC';

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  email VARCHAR(75) UNIQUE,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE todos (
  id INTEGER PRIMARY KEY,
  title VARCHAR(500) NOT NULL,
  is_complete BOOLEAN,
  user_id INTEGER,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT fk_todos_users
    FOREIGN KEY(user_id)
      REFERENCES users(id)
);
